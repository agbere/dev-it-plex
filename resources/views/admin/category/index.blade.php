@extends('admin.category.layouts.app')

@section('content')
    <div class="m-lg-3">
    <h1>Categories</h1><br>
    <p><a href="{{route('category.create')}}" class="btn btn-primary">New category</a></p><br>
    @if(count($categories)>0)
        <table class="table table-bordered table-striped table-sm">
            <thead class="thead-dark">
            <tr>
                <th class="col-sm-2">ID</th>
                <th class="col-sm-3">Nom</th>
                <th class="col-sm-7">Description</th>
            </tr>
            </thead>
            @foreach($categories as $category)
            <tr>
                <td><a href="{{route('category.show', $category->id)}}">{{ $category->id }}</a></td>
                <td><a href="{{route('category.show', $category->id)}}">{{ $category->name }}</a></td>
                <td>{{ $category->description }}</td>
            </tr>
            @endforeach
        </table>
    @else
        <strong>No categories</strong>
    @endif
    </div>

@endsection
