@extends('user.layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">You are logged in USER!</div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                        <a class="btn btn-primary" href="{{route('product.index')}}">Display products</a>
                        <br><br>
                        <a class="btn btn-primary" href="{{route('product.create')}}">New product</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
