@extends('admin.category.layouts.app')

@section('content')
    <h1 >{{$category->name}}</h1>

    <p><strong>{{$category->description}}</strong></p>

    <a href="{{route('category.index')}}" class="btn btn-primary">See all categories</a> |
    <a href="{{route('category.edit', $category->id)}}" class="btn btn-primary">Update</a> |

    <form action="{{route('category.destroy', $category->id)}}" method="POST" class="d-inline-block">
        {{csrf_field()}}
        {{method_field("DELETE")}}
        <input type="submit" value="Delete" class="btn btn-danger">
    </form>

@endsection
