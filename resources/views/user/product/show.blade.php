@extends('user.product.layouts.app')

@section('content')
    <div class="m-lg-3">
    <h1>{{$product->name}}</h1>

    <p><strong>{{$product->description}}</strong></p>

    <a class="btn btn-primary" href="{{route('product.index')}}">See all products</a> |
    <a class="btn btn-primary" href="{{route('product.edit', $product->id)}}">Update</a> |

    <form action="{{route('product.destroy', $product->id)}}" method="POST" class="d-inline-block">
        {{csrf_field()}}
        {{method_field("DELETE")}}
        <input type="submit" value="Delete" class="btn btn-danger">
    </form>
    </div>

@endsection
