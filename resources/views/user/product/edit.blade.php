@extends('user.product.layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Update product - ') }}<strong style="color: darkred"> #{{$product->id}}</strong>
                        <a class="btn btn-primary float-right" href="{{route('product.show', $product)}}">Cancel</a></div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('product.update', $product) }}">
                            {{method_field('PUT')}}
                            @csrf

                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                                <div class="col-md-6">
                                    <input autocomplete="name" autofocus
                                           class="form-control @error('name') is-invalid @enderror"
                                           id="name" name="name"
                                           placeholder="Nouveau nom..." required type="text"
                                           value="{{ old('name') ?: $product->name }}">

                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-4 col-form-label text-md-right" for="description">{{ __('Description') }}</label>

                                <div class="col-md-6">
                                <textarea autocomplete="description" autofocus
                                          class="form-control @error('description') is-invalid @enderror"
                                          id="description"
                                          name="description" placeholder="Nouvelle description..." required
                                          type="text"> {{ old('description') ?: $product->description }}
                                </textarea>

                                    @error('description')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-4 col-form-label text-md-right" for="category_id">{{ __('Category') }}</label>

                                <div class="col-md-6">
                                    <select class="form-control" name="category_id">
                                        @foreach($categories as $category)
                                            <option  value="{{$category->id}}" selected>
                                                {{($product->category_id == $category->id) ? $category->name : $category->name}}
                                            </option>
                                        @endforeach
                                    </select>

                                    @error('category_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="price" class="col-md-4 col-form-label text-md-right">{{ __('Price') }}</label>

                                <div class="col-md-6">
                                    <input form-control id="price" @error('price') is-invalid min="0"
                                           placeholder="New price..."
                                           step="1" type="number" @enderror name="price" value="{{ old('price') ?: $product->price }}" required autocomplete="price" autofocus>

                                    @error('price')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="expire_at" class="col-md-4 col-form-label text-md-right">{{ __('Expiration date') }}</label>

                                <div class="col-md-6">
                                    <input autocomplete="expire_at" autofocus
                                           class="form-control @error('expire_at') is-invalid @enderror"
                                           id="expire_at" name="expire_at"
                                           required type="date" value="{{ old('expire_at') ?: $product->expire_at }}">

                                    @error('expire_at')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>


                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-success">
                                        {{ __('Update') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
