<?php

namespace App\Http\Resources\Api\Admin;

use App\Models\Category;
use App\Http\Resources\Api\Admin\CategoryController as CategoryResource;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Validation\Rule;

class CategoryController extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
       return [
            'name'=>$request->name,
            'description'=>$request->description
        ];

    }

    public function index()
    {
        //return new CategoryResource(Category::find(1));
    }

    protected function create()
    {
        return view('admin.category.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => ['required', 'string', 'min:3', 'max:55', 'unique:categories'],
            'description' => ['required', 'string', 'min:5']
        ]);

        Category::create([
            'name'=>$request->name,
            'description'=>$request->description
        ]);

        return redirect(route('category.index'));
    }

    public function show($id)
    {
       // $category = Category::findOrFail($id);
       // return view('admin.category.show', compact('category'));
    }

    public function edit($id)
    {
        //$category = Category::findOrFail($id);
       // return view('admin.category.edit', compact('category'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => ['required', 'string', 'min:3', 'max:55', Rule::unique('categories')->ignore($id)],
            'description' => ['required', 'string', 'min:5']
        ]);

        Category::findOrFail($id)->update([
            'name'=>$request->name,
            'description'=>$request->description
        ]);

        return redirect(route('category.show', $id));
    }

    public function destroy($id)
    {
       // Category::destroy($id);
       // return redirect(route('category.index'));
    }
}
