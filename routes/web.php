<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//use Illuminate\Support\Facades\Route;

use http\Env\Request;

Route::get('/', function () {
    return view('welcome');
});


Route::get('user/login', function () {
    return view('user.login');
});


Route::get('user/home', function () {
    return view('user.home');
});

Route::get('/admin/home', function () {
    return view('admin.home');
});


//Auth::routes();

Route::get('user/home', 'User\UserHomeController@index')->name('user.home');
Route::get('admin/home', 'Admin\AdminHomeController@index')->name('admin.home');


// Authentication Routes...USER
Route::get('user/login', 'User\Auth\LoginController@showLoginForm')->name('user.login');
Route::post('user/login', 'User\Auth\LoginController@login');
Route::post('logout', 'User\Auth\LoginController@logout')->name('logout');


// Registration Routes...
Route::get('user/register', 'User\Auth\RegisterController@showRegistrationForm')->name('user.register');
Route::post('user/register', 'User\Auth\RegisterController@register');



// Authentication Routes...ADMIN
Route::get('admin/login', 'Admin\Auth\LoginController@showLoginForm')->name('admin.login');
Route::post('admin/login', 'Admin\Auth\LoginController@login');
Route::post('logout', 'Admin\Auth\LoginController@logout')->name('logout');


// Registration Routes...
if ($options['register'] ?? true) {
    Route::get('admin/register', 'Admin\Auth\RegisterController@showRegistrationForm')->name('admin.register');
    Route::post('admin/register', 'Admin\Auth\RegisterController@register');
}
/*
// Password Reset Routes...
if ($options['reset'] ?? true) {
    Route::resetPassword();
}

// Email Verification Routes...
if ($options['verify'] ?? false) {
        Route::emailVerification();
}
*/

Route::resource('user/product', 'User\Product\ProductController')->middleware('auth:user');

Route::resource('admin/category', 'Admin\Category\CategoryController')->middleware('auth:admin');
