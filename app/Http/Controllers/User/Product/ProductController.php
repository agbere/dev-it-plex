<?php

namespace App\Http\Controllers\User\Product;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       // $products = Product::all()->sortBy('id');
        $products = Product::with('category')->get();

        return view('user.product.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        return view('user.product.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => ['required', 'string', 'min:3', 'max:55', 'unique:products'],
            'description' => ['nullable', 'string', 'min:5'],
            'category_id' => ['required', 'int'],
            'price' => ['required', 'int'],
            'expire_at' => ['required', 'date', 'after:'.date('d-m-Y')],
        ]);

        Product::create([
            'name'=>$request->name,
            'description'=>$request->description,
            'category_id'=>$request->category_id,
            'price'=>$request->price,
            'expire_at'=>$request->expire_at
        ]);

        return redirect(route('product.index'));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::findOrFail($id);
        return view('user.product.show', compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categories = Category::all();
        $product = Product::findOrFail($id);

        return view('user.product.edit', compact('product', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => ['required', 'string', 'min:3', 'max:55', Rule::unique('products')->ignore($id)],
            'description' => ['nullable', 'string', 'min:5'],
            'category_id' => ['required', 'int'],
            'price' => ['required', 'int'],
            'expire_at' => ['required', 'date', 'after_or_equal:'.date('d-m-Y')],
        ]);

        Product::findOrFail($id)->update([
            'name'=>$request->name,
            'description'=>$request->description,
            'category_id'=>$request->category_id,
            'price'=>$request->price,
            'expire_at'=>$request->expire_at
        ]);
        return redirect(route('product.show', $id));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Product::destroy($id);

        return redirect(route('product.index'));
    }
}
