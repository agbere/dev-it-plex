@extends('user.product.layouts.app')
@section('content')
    <div class="m-lg-3">
    <h1>Products</h1>
    <p><a href="{{route('product.create')}}" class="btn btn-primary">New product</a></p><br>
    @if(count($products)>0)
        <table class="table table-bordered table-striped table-sm">
            <thead class="thead-dark">
            <tr>
                <th class="col-sm-1">ID</th>
                <th class="col-sm-1">Nom</th>
                <th class="col-sm-3">Description</th>
                <th class="col-sm-1">Category</th>
                <th class="col-sm-1">Price</th>
                <th class="col-sm-1">Expiration date</th>
            </tr>
            </thead>
            @foreach($products as $product)
            <tr>
                <td><a href="{{route('product.show', $product->id)}}">{{ $product->id }}</a></td>
                <td><a href="{{route('product.show', $product->id)}}">{{ $product->name }}</a></td>
                <td>{{ $product->description }}</td>
                <td>{{ $product->category->name }}</td>
                <td>{{ $product->price }}</td>
                <td>{{ $product->expire_at }}</td>
            </tr>
            @endforeach
        </table>
    @else
        <strong>No products</strong>
    @endif
    </div>

@endsection
