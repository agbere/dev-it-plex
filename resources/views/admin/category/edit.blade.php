@extends('admin.category.layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Edit category - ')}}<strong style="color: darkred"> #{{$category->id}}</strong>
                    <a class="btn btn-primary float-right" href="{{route('category.show', $category)}}">Cancel</a>
                    </div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('category.update', $category->id) }}">
                           {{method_field('PUT')}}
                            @csrf

                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                                <div class="col-md-6">
                                    <input autocomplete="name" autofocus class="form-control @error('name') is-invalid @enderror"
                                           id="name" name="name"
                                           placeholder="Nouveau nom..." required type="text"
                                           value="{{ old('name') ?: $category->name }}">

                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-4 col-form-label text-md-right" for="description">{{ __('Description') }}</label>

                                <div class="col-md-6">
                                <textarea autocomplete="description" autofocus
                                          class="form-control @error('description') is-invalid @enderror"
                                          id="description"
                                          name="description" placeholder="Nouvelle description..." required
                                          type="text"> {{ old('description') ?: $category->description }}
                                </textarea>

                                @error('description')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>


                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Update') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
