<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'name', 'description', 'category_id', 'price', 'expire_at',
    ];


    public function category()
    {
       return $this->belongsTo('App\Models\Category') ;
    }

}
