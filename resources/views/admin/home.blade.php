@extends('admin.layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">You are logged in ADMIN!</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                        <a href="{{route('category.index')}}" class="btn btn-primary">Display categories</a>
                        <br><br>
                        <a href="{{route('category.create')}}" class="btn btn-primary "><ion-icon name="add"></ion-icon>New category</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
