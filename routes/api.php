<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('test-get', function (Request $request){
    return response()->json([
        'server_ip' => $request->ip(),
    ]);
});

Route::post('test-post', function (Request $request){
    return response()->json([
        'username' => 'AGBERE',
        'password' => '1232',
    ]);
});


Route::middleware('auth:admin')->post('/admin/register', 'Api\Admin\AuthController@register');
Route::middleware('auth:admin')->post('/admin/login', 'Api\Admin\AuthController@login');

Route::get('/admin/categories', 'Api\Admin\CategoryController@index');
Route::get('/admin/category/{id}/show', 'Api\Admin\CategoryController@show');

Route::post('/admin/category/create', 'Api\Admin\CategoryController@create');
Route::post('/admin/category/{id}/update', 'Api\Admin\CategoryController@update');
