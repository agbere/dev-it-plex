<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string
     */
    protected function redirectTo($request)
    {
        if ($request->is("user/product")||$request->is("user/product/*")){
            if (! $request->expectsJson()) {
                return route('user.login');
            }
        }

        if ($request->is("admin/category")||$request->is("admin/category/*")){
            if (! $request->expectsJson()) {
                return route('admin.login');
            }
        }

       /* if (! $request->expectsJson()) {
            return route('user.login');
        }*/
    }
}
